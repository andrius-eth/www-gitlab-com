- name: Enablement - Section PPI, Stage PPI - Median End User Latency
  base_path: "/handbook/product/performance-indicators/"
  definition: Median end user latency collected by Real User Monitoring. This metric
    captures how well we are doing in serving our primary customers, our end users,
    by measuring their end user experience.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  public: true
  pi_type: Section PPI, Stage PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
      - We know we are [slower than our primary competition](https://dashboards.gitlab.net/d/performance-comparison/github-gitlab-performance?orgId=1)
  implementation:
    status: Instrumentation
    reasons:
    - Working on [implementing RUM on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/218507), currently in progress for 13.5.
    - No target defined, as metric is not instrumented yet
  lessons:
    learned:
    - GitLab.com free usage has [returned to positive growth](https://app.periscopedata.com/app/gitlab/738689/Reduction-in-Free-usage) after the summer lull
    - Self-managed usage [continues to plateau](https://app.periscopedata.com/app/gitlab/738689/Reduction-in-Free-usage)
    - GitLab.com is [accelerating faster](https://app.periscopedata.com/app/gitlab/738689/Reduction-in-Free-usage) than self-managed
    - WIP ["Shift to SaaS" dashboard](https://app.periscopedata.com/app/gitlab/742037/Enablement:-Shift-to-SaaS) to determine whether we are capturing the "lost" growth on .com
    urls:
      - https://gitlab.com/gitlab-data/analytics/-/issues/5657

- name: Enablement:Distribution - Group PPI - Percentage of installations on the 3 most
    recent versions of GitLab
  base_path: "/handbook/product/performance-indicators/"
  definition: Of the total number of self-managed installs, what percentage are on
    one of the three most recent versions.
  target: 40%
  org: Enablement Section
  section: enablement
  stage: enablement
  group: distribution
  public: true
  pi_type: Group PPI 
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - A target of 40% has been set. 40% is the upgrade rate that was reached in early 2019.
    - Insights - The upgrade rate increased in from 25% to 28% in September 2020. This is the first increase since April. However, this is still well below the target of 40%. The steady decline in upgrade rate started in May with the release of 13.0 as the latest release. About 30% of active instances are on the minimum required version of PostgreSQL.
    - Improvement - Implementing additional checks such as free space check during a Postgres update to increase success rate. [New chart](https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=9900400&udv=1102166) to track PostgreSQL upgrade trends.
  implementation:
    status: Complete
    reasons:
    - Primary PPI is complete, and target is set.
    - Instrumentation of some sub metrics is still in progress
  sisense_data:
    chart: 8890933
    dashboard: 406972
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/4791
  - https://gitlab.com/gitlab-data/analytics/-/issues/4800
  - https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=5697571&udv=832292

- name: Enablement:Geo - Paid GMAU - Number of unique users utilizing a Geo secondary
  base_path: "/handbook/product/performance-indicators/"
  definition: Number of unique users utilizing a Geo secondary. This adoption metric
    helps us understand whether end users are actually seeing value in, and are using,
    geo secondaries.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  group: geo
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet, as we don't have instrumentation
    - Insight -  The number of [Geo nodes deployed](https://app.periscopedata.com/app/gitlab/500159/Geo?widget=6472090&udv=0)
      has continued to grow over second half of 2020. We know that Geo has low
      penetration as a percentage of total deployments, but skews heavily toward the
      large enterprise with a [25% percentage of Premium+ user share](https://docs.google.com/presentation/d/1imw_PWKZhJpxRs_VwTa-SWgwbZJhw2jKp6wMRk2Fo3c/edit#slide=id.g807f195cca_0_768).
    - Improvement - We are working to add support for replicating all data types,
      so that Geo is on a solid foundation for both DR and Geo Replication.
  implementation:
    status: Instrumentation
    reasons:
    - Geo is not available on GitLab.com today, so cannot use Snowplow or the .com
      database.
    - Ability for Geo secondaries to [support usage ping](https://gitlab.com/gitlab-org/gitlab/-/issues/231257) is underway in 13.5.
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/231257

- name: Enablement:Memory - Group PPI - Requests / Compute
  base_path: "/handbook/product/performance-indicators/"
  definition: Requests per Hour (Rolling 7 day average) / Compute Cost. This metric
    is a measure of efficiency, measuring how many requests GitLab can service for
    one compute dollar. As GitLab is more efficient with CPU and Memory, it will go
    up.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  group: memory
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  sisense_data:
    chart: 9627495
    dashboard: 679200
    embed: v2
  health:
    level: 1
    reasons:
    - No target defined yet, as we only have 2 datapoint and variance is quite high. 
    - Improvement - We are working to [resize images](https://gitlab.com/groups/gitlab-org/-/epics/3979)
      which is completed in 13.5 and we just rolled out to all of GitLab.com on 2020-10-05.
  implementation:
    status: Instrumentation
    reasons:
    - Given the extremely high variance, this metric is unlikely to be valuable.
    - Working to better support topology metrics for [multi-node instances](https://gitlab.com/groups/gitlab-org/-/epics/3576) and [Kubernetes](https://gitlab.com/groups/gitlab-org/-/epics/3577)
  lessons:
    learned:
    - There is a significant number of underutilized GitLab instances. Conformance to reference architectures will help, but clearly people are willing to heavily over-provision single node instances. (Single-node is all we have this instrumentation available for today)
    - Due to extremely high variance, unlikely to be valuable for tracking actual memory reductions or performance improvements. Discussing a shift to more specific [memory metrics](https://gitlab.com/gitlab-org/memory-team/team-tasks/-/issues/66)
  sisense_data:
      chart: 9875780
      dashboard: 753991
      embed: v2
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/230898

- name: Enablement:Global Search - Paid GMAU - The number of unique paid
    users per month
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique active users and unique paid users interacting with either Basic Search or Advanced Search per month.
  target: Establishing Baseline
  org: Enablement Section
  section: enablement
  stage: enablement
  group: global_search
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - Establishing Baseline   
  implementation:
    status: Dashboard
    reasons:
    - SaaS data is partialy collected in September 2020
    - Self-managed data is implemented in 13.4
  sisense_data:
      chart: 9908934
      dashboard: 596072
      embed: v2

- name: Enablement:Database - Group PPI - Database Query Apdex (100ms target, 250ms tolerable)
  base_path: "/handbook/product/performance-indicators/"
  definition: Database query Apdex, with 100ms the target and 250ms tolerable. This
    measures the ratio of queries which complete within the satisfactory time, informing
    how well the database is scaling and performing.
  target: 0.99
  org: Enablement Section
  section: enablement
  stage: enablement
  group: database
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Improvement - We are focusing on [partitioning](https://gitlab.com/groups/gitlab-org/-/epics/2023) 
      the largest tables to improveme the performance and scalability of the database.
  implementation:
    status: Complete
    reasons:
    - The [PPI has been instrumented](https://gitlab.com/gitlab-org/gitlab/-/issues/227305) in 13.4, but data is just starting to come back.
  sisense_data:
    chart: 9885641
    dashboard: 754160
    embed: v2
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/227305

- name: Enablement:Infrastructure - Paid GMAU - Number of unique paid users that perform
    an action on GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The sum of all monthly active users on gitlab.com that roll up to paid
    accounts in a 28 day rolling period.
  target: 96000
  org: Enablement Section
  section: enablement
  stage: enablement
  group: infrastructure
  public: true
  pi_type: Paid GMAU
  product_analytics_type: SaaS
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - On track for Q3 Target
  implementation:
    status: Complete
    reasons:
    - Instrumentation complete.
  sisense_data:
    chart: 9655306
    dashboard: 710777
    embed: v2
