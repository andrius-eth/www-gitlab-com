---
layout: handbook-page-toc
title: "Application Security"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Application Security Mission

The application security team works closely with engineering and product teams
to ensure that all GitLab products securely handle the customer data with
which we are entrusted.

## Role Functions

### Stable Counterparts

Please see the [Application Security Stable Counterparts page][4].

### Application Security Reviews

Please see the [Application Security Reviews page][1].

## Project ownership

Please see the [Appsec project owners page][2]

## Application Security Engineer Runbooks

Please see the [Application Security Engineer Runbooks page index][5]

## Meeting Recordings

The following recordings are available internally only:

* [AppSec Sync](https://drive.google.com/drive/folders/1sxnBhPNDofWg5JmKqrhEl5y4_aWldTbt)
* [AppSec Leadership Weekly](https://drive.google.com/drive/folders/1jyNYP2AOqoOPqr4qGMuh7PGha_j-7brb)

[1]: /handbook/engineering/security/application-security/appsec-reviews.html
[2]: /handbook/engineering/security/application-security/project-owners.html
[3]: /handbook/engineering/security/application-security/vulnerability-management.html
[4]: /handbook/engineering/security/application-security/stable-counterparts.html
[5]: /handbook/engineering/security/application-security/runbooks

## Backlog reviews

When necessary a backlog review can be initiated, please see the [Vulnerability Management Page][3] for more details.
