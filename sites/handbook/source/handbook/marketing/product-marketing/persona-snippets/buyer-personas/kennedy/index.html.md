---
layout: handbook-page-toc
title: "Kennedy Persona snippets"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Use case specific persona snippets

### [Kennedy (Infrastructure Engineering Director)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/buyer-persona/#kennedy---the-infrastructure-engineering-director)

**Overview**
- description

#### [VC&C use case](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/version-control-collaboration/#personas)

##### SDR Discovery Questions

- ...

##### SDR Use Case Snippets

- ...

#### [CI use case](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/ci/#personas)

##### SDR Discovery Questions

- ...

##### SDR Use Case Snippets

- ...

#### [CD use case](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/cd/#personas)

##### SDR Discovery Questions

- ...

##### SDR Use Case Snippets

- ...

#### [DevSecOps use case](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/devsecops/#personas)

##### SDR Discovery Questions

- ...

##### SDR Use Case Snippets

- ...
