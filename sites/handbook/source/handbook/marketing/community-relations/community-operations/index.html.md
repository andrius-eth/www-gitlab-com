---
layout: handbook-page-toc
title: "Community Operations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Our mission

GitLab's Community Operations Program is responsible for developing and maintaining the infrastructure and resources to support the Community Relations team and the GitLab community at large. In this, we help support GitLab's greater mission of 'everyone can contribute' by encouraging the wider GitLab community through process and opportunity.  

### Partnership

The mission of this program is to act as a partner to all Program Managers in the Community Relations team to define, implement and refine KPIs/PIs to measure and report the success and effectiveness of our community programs. We work together with the Community Relations team’s Program Managers to produce regular, engaging content to highlight their programs and attract new contributors. 

The Community Operations function also works closely with the Marketing Operations, and Data and Analytics teams. 

### Tooling

Community Operations defines and maintains the tool stack required to measure and interact with the wider GitLab community.
The Community Program Manager acts as the DRI for the Community Relations team’s webpages on about.gitlab.com. This program also supports the Open Source and Education teams by processing program applications and renewals. Ultimately, we are working towards a process to fully automate this.

### GitLab's Community

The Community Operations Program curates and maintains documentation for any team member to productively engage with the wider community. When necessary, we engage with specialists within GitLab to provide responses and listen to our community’s feedback on [The GitLab forum](forum.gitlab.com), the [GitLab blog](blog.gitlab.com) and Hackernews.

The [Community Operations Manager](https://about.gitlab.com/handbook/marketing/community-relations/#who-we-are) reports to the [Director of the Community Relations Team](https://about.gitlab.com/job-families/marketing/director-of-community-relations/). 

## Community Operations Work

You can find the Community Operations Program peppered throughout the Community Relations handbook, processes, and projects. 

In order to loop in Community Operations on GitLab.com, please use the `community-ops` label. 

Everything labeled `community-ops` is organized on the [Community Operations Issue Board](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/2062229?assignee_username=LindsayOlson&label_name[]=Community%20Ops). 

### How to Use the Community Operations Issue Board

| Column Name | Description |
|-------------|-------------|
| Open        | All open issues with the `community-ops-` label   |
| Todo        | Issues with a due date or high priority   |
| Doing       | Issues that are currently in-flight (these have action items for the Community Operations Manager)   |
| Closed      | Issues that have been closed or completed   |

### How to Use the community-ops Label in GitLab

Please use the `community-ops` label only when there is an action item needed from the Community Operations Manager. 

If you think an issue or MR is a "nice to know" for the Community Operations Manager, feel free to loop them in (@lindsayolson) via a comment instead. 


## Community Operations Response Channels

| Channel Name | Source      | Action.     |
|--------------|-------------|-------------|
| HackerNews    | Hacker News mentions via Zapier   | Find an expert, and collaborate with the Developer Evangelism Team in Slack |
| Hacker News front page stories    | Hacker News mentions via Zapier   | Find an expert, and collaborate with the Developer Evangelism Team in Slack |
| GitLab Forum    | Discourse (forum.gitlab.com)   | Find an expert in the Forum Contributors Group, and collaborate in Slack |
| GitLab Blog    | Disqus (blog.gitlab.com)   | Find an expert (usually the blog post author), and collaborate in Slack |

## Relevant Links

[Social Media Guidelines](https://about.gitlab.com/handbook/marketing/social-media-guidelines/)
[Community Relations Handbook](https://about.gitlab.com/handbook/marketing/community-relations/)
 
